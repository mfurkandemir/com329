# Microservice application written in Golang

This application is written in Golang and with a microservice architecture. And it just returns the count of how many characters in your request.
request data.

Before you run it, you should know that Golang should be installed and you should be using a Gnu/Linux distro and last thing is installing go-kit by:

```
$ go mod download github.com/go-kit/kit
$ go get github.com/go-kit/kit/log@v0.10.0
````

## Usage

1. Run the application
```
$ go run main.go
```
2. Open new tab in terminal
3. Send request to application

```
$ curl -X POST -d '{"s":"mfurkandemir-b1605010052"}' 0.0.0.0:8080/count
```
